﻿namespace Superfarmer
{
    partial class Cheats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelHeader = new System.Windows.Forms.Label();
            this.labelWarning = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonDiscard = new System.Windows.Forms.Button();
            this.checkBoxSmallDog = new System.Windows.Forms.CheckBox();
            this.checkBoxBigDog = new System.Windows.Forms.CheckBox();
            this.nudHorses = new System.Windows.Forms.NumericUpDown();
            this.nudCows = new System.Windows.Forms.NumericUpDown();
            this.nudPigs = new System.Windows.Forms.NumericUpDown();
            this.nudSheep = new System.Windows.Forms.NumericUpDown();
            this.nudRabbits = new System.Windows.Forms.NumericUpDown();
            this.labelRabbit = new System.Windows.Forms.Label();
            this.labelSheep = new System.Windows.Forms.Label();
            this.labelPig = new System.Windows.Forms.Label();
            this.labelCow = new System.Windows.Forms.Label();
            this.labelHorse = new System.Windows.Forms.Label();
            this.labelSmallDog = new System.Windows.Forms.Label();
            this.labelBigDog = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudHorses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCows)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPigs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSheep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRabbits)).BeginInit();
            this.SuspendLayout();
            // 
            // labelHeader
            // 
            this.labelHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelHeader.Location = new System.Drawing.Point(12, 9);
            this.labelHeader.Name = "labelHeader";
            this.labelHeader.Size = new System.Drawing.Size(266, 27);
            this.labelHeader.TabIndex = 6;
            this.labelHeader.Text = "Edit starting animals for player ";
            this.labelHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelWarning
            // 
            this.labelWarning.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWarning.Location = new System.Drawing.Point(12, 247);
            this.labelWarning.Name = "labelWarning";
            this.labelWarning.Size = new System.Drawing.Size(266, 23);
            this.labelWarning.TabIndex = 7;
            this.labelWarning.Text = "For fair play, you should not edit those values.";
            this.labelWarning.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSave.Location = new System.Drawing.Point(12, 273);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 1;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // buttonDiscard
            // 
            this.buttonDiscard.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDiscard.Location = new System.Drawing.Point(203, 273);
            this.buttonDiscard.Name = "buttonDiscard";
            this.buttonDiscard.Size = new System.Drawing.Size(75, 23);
            this.buttonDiscard.TabIndex = 0;
            this.buttonDiscard.Text = "Discard";
            this.buttonDiscard.UseVisualStyleBackColor = true;
            this.buttonDiscard.Click += new System.EventHandler(this.ButtonDiscard_Click);
            // 
            // checkBoxSmallDog
            // 
            this.checkBoxSmallDog.AutoSize = true;
            this.checkBoxSmallDog.Location = new System.Drawing.Point(131, 187);
            this.checkBoxSmallDog.Name = "checkBoxSmallDog";
            this.checkBoxSmallDog.Size = new System.Drawing.Size(15, 14);
            this.checkBoxSmallDog.TabIndex = 8;
            this.checkBoxSmallDog.UseVisualStyleBackColor = true;
            // 
            // checkBoxBigDog
            // 
            this.checkBoxBigDog.AutoSize = true;
            this.checkBoxBigDog.Location = new System.Drawing.Point(131, 210);
            this.checkBoxBigDog.Name = "checkBoxBigDog";
            this.checkBoxBigDog.Size = new System.Drawing.Size(15, 14);
            this.checkBoxBigDog.TabIndex = 9;
            this.checkBoxBigDog.UseVisualStyleBackColor = true;
            // 
            // nudHorses
            // 
            this.nudHorses.Location = new System.Drawing.Point(131, 161);
            this.nudHorses.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nudHorses.Name = "nudHorses";
            this.nudHorses.Size = new System.Drawing.Size(80, 20);
            this.nudHorses.TabIndex = 10;
            // 
            // nudCows
            // 
            this.nudCows.Location = new System.Drawing.Point(131, 135);
            this.nudCows.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nudCows.Name = "nudCows";
            this.nudCows.Size = new System.Drawing.Size(80, 20);
            this.nudCows.TabIndex = 11;
            // 
            // nudPigs
            // 
            this.nudPigs.Location = new System.Drawing.Point(131, 109);
            this.nudPigs.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nudPigs.Name = "nudPigs";
            this.nudPigs.Size = new System.Drawing.Size(80, 20);
            this.nudPigs.TabIndex = 12;
            // 
            // nudSheep
            // 
            this.nudSheep.Location = new System.Drawing.Point(131, 83);
            this.nudSheep.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nudSheep.Name = "nudSheep";
            this.nudSheep.Size = new System.Drawing.Size(80, 20);
            this.nudSheep.TabIndex = 13;
            // 
            // nudRabbits
            // 
            this.nudRabbits.Location = new System.Drawing.Point(131, 57);
            this.nudRabbits.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nudRabbits.Name = "nudRabbits";
            this.nudRabbits.Size = new System.Drawing.Size(80, 20);
            this.nudRabbits.TabIndex = 14;
            // 
            // labelRabbit
            // 
            this.labelRabbit.Location = new System.Drawing.Point(25, 54);
            this.labelRabbit.Name = "labelRabbit";
            this.labelRabbit.Size = new System.Drawing.Size(100, 23);
            this.labelRabbit.TabIndex = 15;
            this.labelRabbit.Text = "rabbit";
            this.labelRabbit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelSheep
            // 
            this.labelSheep.Location = new System.Drawing.Point(25, 80);
            this.labelSheep.Name = "labelSheep";
            this.labelSheep.Size = new System.Drawing.Size(100, 23);
            this.labelSheep.TabIndex = 16;
            this.labelSheep.Text = "sheep";
            this.labelSheep.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPig
            // 
            this.labelPig.Location = new System.Drawing.Point(25, 106);
            this.labelPig.Name = "labelPig";
            this.labelPig.Size = new System.Drawing.Size(100, 23);
            this.labelPig.TabIndex = 17;
            this.labelPig.Text = "pig";
            this.labelPig.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCow
            // 
            this.labelCow.Location = new System.Drawing.Point(25, 132);
            this.labelCow.Name = "labelCow";
            this.labelCow.Size = new System.Drawing.Size(100, 23);
            this.labelCow.TabIndex = 18;
            this.labelCow.Text = "cow";
            this.labelCow.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelHorse
            // 
            this.labelHorse.Location = new System.Drawing.Point(25, 158);
            this.labelHorse.Name = "labelHorse";
            this.labelHorse.Size = new System.Drawing.Size(100, 23);
            this.labelHorse.TabIndex = 19;
            this.labelHorse.Text = "horse";
            this.labelHorse.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelSmallDog
            // 
            this.labelSmallDog.Location = new System.Drawing.Point(25, 182);
            this.labelSmallDog.Name = "labelSmallDog";
            this.labelSmallDog.Size = new System.Drawing.Size(100, 23);
            this.labelSmallDog.TabIndex = 20;
            this.labelSmallDog.Text = "small dog";
            this.labelSmallDog.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelBigDog
            // 
            this.labelBigDog.Location = new System.Drawing.Point(25, 205);
            this.labelBigDog.Name = "labelBigDog";
            this.labelBigDog.Size = new System.Drawing.Size(100, 23);
            this.labelBigDog.TabIndex = 21;
            this.labelBigDog.Text = "big dog";
            this.labelBigDog.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Cheats
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(290, 308);
            this.ControlBox = false;
            this.Controls.Add(this.labelBigDog);
            this.Controls.Add(this.labelSmallDog);
            this.Controls.Add(this.labelHorse);
            this.Controls.Add(this.labelCow);
            this.Controls.Add(this.labelPig);
            this.Controls.Add(this.labelSheep);
            this.Controls.Add(this.labelRabbit);
            this.Controls.Add(this.nudRabbits);
            this.Controls.Add(this.nudSheep);
            this.Controls.Add(this.nudPigs);
            this.Controls.Add(this.nudCows);
            this.Controls.Add(this.nudHorses);
            this.Controls.Add(this.checkBoxBigDog);
            this.Controls.Add(this.checkBoxSmallDog);
            this.Controls.Add(this.buttonDiscard);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.labelWarning);
            this.Controls.Add(this.labelHeader);
            this.MaximumSize = new System.Drawing.Size(306, 347);
            this.MinimumSize = new System.Drawing.Size(306, 347);
            this.Name = "Cheats";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cheats";
            ((System.ComponentModel.ISupportInitialize)(this.nudHorses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCows)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPigs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSheep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRabbits)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelHeader;
        private System.Windows.Forms.Label labelWarning;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonDiscard;
        private System.Windows.Forms.CheckBox checkBoxSmallDog;
        private System.Windows.Forms.CheckBox checkBoxBigDog;
        private System.Windows.Forms.NumericUpDown nudHorses;
        private System.Windows.Forms.NumericUpDown nudCows;
        private System.Windows.Forms.NumericUpDown nudPigs;
        private System.Windows.Forms.NumericUpDown nudSheep;
        private System.Windows.Forms.NumericUpDown nudRabbits;
        private System.Windows.Forms.Label labelRabbit;
        private System.Windows.Forms.Label labelSheep;
        private System.Windows.Forms.Label labelPig;
        private System.Windows.Forms.Label labelCow;
        private System.Windows.Forms.Label labelHorse;
        private System.Windows.Forms.Label labelSmallDog;
        private System.Windows.Forms.Label labelBigDog;
    }
}