﻿using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;


namespace Superfarmer
{
    class MarketPictureBox : PictureBox
    {
        public bool active = false;

        public bool Active
        {
            get { return active; }
            set
            {
                Image = value ? ActiveImage : InactiveImage;
                active = value;
            }
        }
        [Category("Values"),
        Browsable(true)]
        public Image ActiveImage { get; set; }
        [Category("Values"),
        Browsable(true)]
        public Image InactiveImage { get; set; }

        public enum Animals { other, rabbit, sheep, pig, cow, horse }
        [Category("Values"),
         Browsable(true)]
        public Animals AnimalType { get; set; }
        [Category("Values"),
         Browsable(true)]
        public int AnimalQuantity { get; set; }
        [Category("Values"),
         Browsable(true)]
        public MarketPictureBox AnimalToSell { get; set; }

        public enum DogT { no, smallDogBuy, smallDogSell, bigDogBuy, bigDogSell }
        [Category("Values"),
         Browsable(true)]
        public DogT Dog { get; set; }
    }
}
