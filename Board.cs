﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Superfarmer
{
    public partial class Board : Form
    {
        enum Dice1 { none = 0, rabbit = 1, sheep = 2, pig = 3, cow = 4, wolf = 7 };
        enum Dice2 { none = 0, rabbit = 1, sheep = 2, pig = 3, horse = 5, fox = 6 };
        Dice1 dice1 = Dice1.none;
        Dice2 dice2 = Dice2.none;
        bool rolled = false;

        int numberOfPlayers;
        string[] playerNames;
        PlayerAnimals[] playerAnimals;
        int playerTurn = 0;

        #region Init
        public Board(int numberOfPlayersSelected, string[] players, Cheats[] startingAnimals)
        {
            InitializeComponent();
            this.AcceptButton = buttonQuit;

            playerNames = players;

            numberOfPlayers = numberOfPlayersSelected;
            playerAnimals = new PlayerAnimals[numberOfPlayers];

            playerAnimals[0] = playerTable;
            playerAnimals[0].PlayerName.Text = playerNames[0];

            Point location = playerTable.Location;
            int increment = playerTable.Size.Width + 5;
            for (int i = 1; i < numberOfPlayers; i++)
            {
                playerAnimals[i] = new PlayerAnimals();
                Label name = playerAnimals[i].PlayerName;
                name.Text = playerNames[i];
                while (name.Width < TextRenderer.MeasureText(name.Text, new Font(name.Font.FontFamily, name.Font.Size, name.Font.Style)).Width)
                    name.Font = new Font(name.Font.FontFamily, name.Font.Size - 0.5f, name.Font.Style);
                location.X += increment;
                playerAnimals[i].Location = location;
                this.Size = new Size(this.Size.Width + increment, this.Size.Height);
                this.Controls.Add(playerAnimals[i]);
            }
            for (int i = 0; i < numberOfPlayers; i++)
            {
                playerAnimals[i].Rabbit = startingAnimals[i].Rabbits;
                playerAnimals[i].Sheep = startingAnimals[i].Sheep;
                playerAnimals[i].Pig = startingAnimals[i].Pigs;
                playerAnimals[i].Cow = startingAnimals[i].Cows;
                playerAnimals[i].Horse = startingAnimals[i].Horses;
                playerAnimals[i].SmallDog = startingAnimals[i].SmallDog;
                playerAnimals[i].BigDog = startingAnimals[i].BigDog;
            }
            this.MinimumSize = this.Size;

            SetupBoard();
        }

        void SetupBoard()
        {
            this.Text = Properties.Strings.applicationTitle;
            playerAnimals[playerTurn].PlayerNameColors(Color.Black, Color.Silver);
            labelMarketL1.Text = Convert.ToString(mpbMarketL1.AnimalQuantity);
            labelMarketL2.Text = Convert.ToString(mpbMarketL2.AnimalQuantity);
            labelMarketL3.Text = Convert.ToString(mpbMarketL3.AnimalQuantity);
            labelMarketL4.Text = Convert.ToString(mpbMarketL4.AnimalQuantity);
            labelMarketL5.Text = Convert.ToString(mpbMarketL5.AnimalQuantity);
            labelMarketL6.Text = Convert.ToString(mpbMarketL6.AnimalQuantity);
            labelMarketR1.Text = Convert.ToString(mpbMarketR1.AnimalQuantity);
            labelMarketR2.Text = Convert.ToString(mpbMarketR2.AnimalQuantity);
            labelMarketR3.Text = Convert.ToString(mpbMarketR3.AnimalQuantity);
            labelMarketR4.Text = Convert.ToString(mpbMarketR4.AnimalQuantity);
            labelMarketR5.Text = Convert.ToString(mpbMarketR5.AnimalQuantity);
            labelMarketR6.Text = Convert.ToString(mpbMarketR6.AnimalQuantity);
            buttonRoll.Text = Properties.Strings.buttonDiceRoll;
            buttonRules.Text = Properties.Strings.buttonRules;
            buttonRestart.Text = Properties.Strings.buttonRestart;
            buttonQuit.Text = Properties.Strings.buttonQuit;
        }
        #endregion

        #region Dice Actions
        void ButtonRollClick(object sender, EventArgs e)
        {
            if (rolled) PassDice();
            else RollDice();

            DiceUpdate();
            MarketUpdate();
        }

        void PassDice()
        {
            playerAnimals[playerTurn].PlayerNameColors(Color.Silver, Color.White);

            GameOverCheck();

            playerTurn = (playerTurn != numberOfPlayers - 1) ? playerTurn + 1 : 0;
            playerAnimals[playerTurn].PlayerNameColors(Color.Black, Color.Silver);

            dice1 = Dice1.none;
            dice2 = Dice2.none;

            buttonRoll.Text = Properties.Strings.buttonDiceRoll;

            rolled = false;
        }

        void RollDice()
        {
            const int lowerLimit = 1;
            const int upperLimit = 12;
            var random = new Random();
            int dice1Roll = random.Next(lowerLimit, upperLimit + 1);
            int dice2Roll = random.Next(lowerLimit, upperLimit + 1);

            switch (dice1Roll)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    dice1 = Dice1.rabbit;
                    break;
                case 7:
                case 8:
                case 9:
                    dice1 = Dice1.sheep;
                    break;
                case 10:
                    dice1 = Dice1.pig;
                    break;
                case 11:
                    dice1 = Dice1.cow;
                    break;
                case 12:
                    dice1 = Dice1.wolf;
                    break;
                default:
                    dice1 = Dice1.none;
                    break;
            }
            switch (dice2Roll)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    dice2 = Dice2.rabbit;
                    break;
                case 7:
                case 8:
                    dice2 = Dice2.sheep;
                    break;
                case 9:
                case 10:
                    dice2 = Dice2.pig;
                    break;
                case 11:
                    dice2 = Dice2.horse;
                    break;
                case 12:
                    dice2 = Dice2.fox;
                    break;
                default:
                    dice2 = Dice2.none;
                    break;
            }

            for (int i = 1; i <= 5; i++)
            {
                int currentCount = playerAnimals[playerTurn].GetByID(i);
                if ((int)dice1 == (i) && (int)dice2 == (i))
                {
                    int result = currentCount + ((currentCount + 2) / 2);
                    playerAnimals[playerTurn].SetByID(i, result);
                }
                else if ((int)dice1 == (i) || (int)dice2 == (i))
                {
                    int result = currentCount + ((currentCount + 1) / 2);
                    playerAnimals[playerTurn].SetByID(i, result);
                }
            }

            if (dice2 == Dice2.fox)
            {
                if (playerAnimals[playerTurn].SmallDog)
                    playerAnimals[playerTurn].SmallDog = false;
                else
                    playerAnimals[playerTurn].Rabbit = 0;
            }

            if (dice1 == Dice1.wolf)
            {
                if (playerAnimals[playerTurn].BigDog)
                    playerAnimals[playerTurn].BigDog = false;
                else
                {
                    playerAnimals[playerTurn].Rabbit = 0;
                    playerAnimals[playerTurn].Sheep = 0;
                    playerAnimals[playerTurn].Cow = 0;
                    playerAnimals[playerTurn].Pig = 0;
                }
            }

            playerAnimals[playerTurn].PlayerNameColors(Color.Black, Color.White);
            buttonRoll.Text = Properties.Strings.buttonDiceContinue;

            rolled = true;
        }

        void DiceUpdate()
        {
            switch (dice1)
            {
                case Dice1.rabbit:
                    labelDice1.Text = Properties.Strings.animalRabbit;
                    pbDice1.Image = Properties.Resources.big_rabbit;
                    break;
                case Dice1.sheep:
                    labelDice1.Text = Properties.Strings.animalSheep;
                    pbDice1.Image = Properties.Resources.big_sheep;
                    break;
                case Dice1.pig:
                    labelDice1.Text = Properties.Strings.animalPig;
                    pbDice1.Image = Properties.Resources.big_pig;
                    break;
                case Dice1.cow:
                    labelDice1.Text = Properties.Strings.animalCow;
                    pbDice1.Image = Properties.Resources.big_cow;
                    break;
                case Dice1.wolf:
                    labelDice1.Text = Properties.Strings.animalWolf;
                    pbDice1.Image = Properties.Resources.big_wolf;
                    break;
                default:
                    labelDice1.Text = "- - -";
                    pbDice1.Image = Properties.Resources.big_dice;
                    break;
            }

            switch (dice2)
            {
                case Dice2.rabbit:
                    labelDice2.Text = Properties.Strings.animalRabbit;
                    pbDice2.Image = Properties.Resources.big_rabbit;
                    break;
                case Dice2.sheep:
                    labelDice2.Text = Properties.Strings.animalSheep;
                    pbDice2.Image = Properties.Resources.big_sheep;
                    break;
                case Dice2.pig:
                    labelDice2.Text = Properties.Strings.animalPig;
                    pbDice2.Image = Properties.Resources.big_pig;
                    break;
                case Dice2.horse:
                    labelDice2.Text = Properties.Strings.animalHorse;
                    pbDice2.Image = Properties.Resources.big_horse;
                    break;
                case Dice2.fox:
                    labelDice2.Text = Properties.Strings.animalFox;
                    pbDice2.Image = Properties.Resources.big_fox;
                    break;
                default:
                    labelDice2.Text = "- - -";
                    pbDice2.Image = Properties.Resources.big_dice;
                    break;
            }

            if (rolled)
            {
                if ((int)dice1 == (int)dice2)
                {
                    labelDice1.BackColor = Color.LimeGreen;
                    labelDice2.BackColor = Color.LimeGreen;
                }
                else
                {
                    labelDice1.BackColor = dice1 == Dice1.wolf ? Color.IndianRed : Color.SkyBlue;
                    labelDice2.BackColor = dice2 == Dice2.fox ? Color.IndianRed : Color.SkyBlue;
                }
            }
            else
            {
                labelDice1.BackColor = Color.Silver;
                labelDice2.BackColor = Color.Silver;
            }
        }
        #endregion

        #region Market
        void PBMarketClick(object sender, EventArgs e)
        {
            if (!rolled)
            {
                MarketPictureBox clicked = (MarketPictureBox)sender;
                if (clicked.Active)
                {
                    MarketTransaction(clicked);
                    MarketUpdate();
                    GameOverCheck();
                }
            }

        }

        void MarketTransaction(MarketPictureBox mpb)
        {
            if (mpb.Dog == MarketPictureBox.DogT.no)
            {
                Buy();
                Sell();
            }
            else if (mpb.Dog == MarketPictureBox.DogT.smallDogSell)
            {
                Buy();
                playerAnimals[playerTurn].SmallDog = false;
            }
            else if (mpb.Dog == MarketPictureBox.DogT.smallDogBuy)
            {
                playerAnimals[playerTurn].SmallDog = true;
                Sell();
            }
            else if (mpb.Dog == MarketPictureBox.DogT.bigDogSell)
            {
                Buy();
                playerAnimals[playerTurn].BigDog = false;
            }
            else if (mpb.Dog == MarketPictureBox.DogT.bigDogBuy)
            {
                playerAnimals[playerTurn].BigDog = true;
                Sell();
            }

            void Buy()
            {
                int animal = (int)mpb.AnimalType;
                int animalQuantity = mpb.AnimalQuantity;
                int currentCount = playerAnimals[playerTurn].GetByID(animal);
                playerAnimals[playerTurn].SetByID(animal, currentCount + animalQuantity);

            }
            void Sell()
            {
                int animal = (int)mpb.AnimalToSell.AnimalType;
                int animalQuantity = mpb.AnimalToSell.AnimalQuantity;
                int currentCount = playerAnimals[playerTurn].GetByID(animal);
                playerAnimals[playerTurn].SetByID(animal, currentCount - animalQuantity);
            }
        }

        void MarketUpdate()
        {
            CheckActive(mpbMarketL1);
            CheckActive(mpbMarketL2);
            CheckActive(mpbMarketL3);
            CheckActive(mpbMarketL4);
            CheckActive(mpbMarketL5);
            CheckActive(mpbMarketL6);
            CheckActive(mpbMarketR1);
            CheckActive(mpbMarketR2);
            CheckActive(mpbMarketR3);
            CheckActive(mpbMarketR4);
            CheckActive(mpbMarketR5);
            CheckActive(mpbMarketR6);

            void CheckActive(MarketPictureBox mpb)
            {
                if (mpb.Dog == MarketPictureBox.DogT.no)
                    mpb.Active = CheckEnough(mpb.AnimalToSell) && CheckNotTooMuch(mpb) && !rolled;
                else if (mpb.Dog == MarketPictureBox.DogT.smallDogSell)
                    mpb.Active = playerAnimals[playerTurn].SmallDog && !rolled;
                else if (mpb.Dog == MarketPictureBox.DogT.smallDogBuy)
                    mpb.Active = CheckEnough(mpb.AnimalToSell) && !playerAnimals[playerTurn].SmallDog && !rolled;
                else if (mpb.Dog == MarketPictureBox.DogT.bigDogSell)
                    mpb.Active = playerAnimals[playerTurn].BigDog && !rolled;
                else if (mpb.Dog == MarketPictureBox.DogT.bigDogBuy)
                    mpb.Active = CheckEnough(mpb.AnimalToSell) && !playerAnimals[playerTurn].BigDog && !rolled;
            }
            bool CheckEnough(MarketPictureBox mpb)
            {
                return playerAnimals[playerTurn].GetByID((int)mpb.AnimalType) >= mpb.AnimalQuantity;
            }
            bool CheckNotTooMuch(MarketPictureBox mpb)
            {
                return playerAnimals[playerTurn].GetByID((int)mpb.AnimalType) != playerAnimals[playerTurn].maxNumber;
            }
        }
        #endregion

        void GameOverCheck()
        {
            bool gameOver = true;
            for (int i = 1; i <= 5; i++)
            {
                gameOver = gameOver && playerAnimals[playerTurn].GetByID(i) >= 1;
            }
            if (gameOver)
            {
                DialogResult konec = MessageBox.Show(playerNames[playerTurn] + Properties.Strings.gameOverDialogText + Environment.NewLine + Environment.NewLine + Properties.Strings.gameOverDialogText2,
                                                     Properties.Strings.gameOverDialog, MessageBoxButtons.YesNo);
                if (konec == DialogResult.Yes) Application.Restart();
                else Application.Exit();
            }
        }

        #region Controls
        //Controls
        void ButtonQuitClick(object sender, EventArgs e)
        {
            DialogResult vypnout = MessageBox.Show(Properties.Strings.dialogQuitText, Properties.Strings.buttonQuit, MessageBoxButtons.YesNo);
            if (vypnout == DialogResult.Yes) Application.Exit();
        }

        void ButtonRestartClick(object sender, EventArgs e)
        {
            DialogResult restartovat = MessageBox.Show(Properties.Strings.dialogRestartText, Properties.Strings.buttonRestart, MessageBoxButtons.YesNo);
            if (restartovat == DialogResult.Yes) Application.Restart();
        }

        void ButtonRulesClick(object sender, EventArgs e)
        {
            Rules rules = new Rules();
            rules.Show();
        }
        #endregion
    }
}