﻿namespace Superfarmer
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.labelTitle = new System.Windows.Forms.Label();
            this.linkLabelWiki = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.labelPlayer1 = new System.Windows.Forms.Label();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.labelPlayer2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.labelPlayer3 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.labelPlayer4 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.labelPlayer5 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.pbLanguageCzech = new System.Windows.Forms.PictureBox();
            this.pbLanguageEnglish = new System.Windows.Forms.PictureBox();
            this.buttonRules = new System.Windows.Forms.Button();
            this.buttonPlay = new System.Windows.Forms.Button();
            this.buttonQuit = new System.Windows.Forms.Button();
            this.labelCheatAlert5 = new System.Windows.Forms.Label();
            this.labelCheatAlert4 = new System.Windows.Forms.Label();
            this.labelCheatAlert3 = new System.Windows.Forms.Label();
            this.labelCheatAlert2 = new System.Windows.Forms.Label();
            this.labelCheatAlert1 = new System.Windows.Forms.Label();
            this.buttonCheats5 = new Superfarmer.ButtonCheats();
            this.buttonCheats4 = new Superfarmer.ButtonCheats();
            this.buttonCheats3 = new Superfarmer.ButtonCheats();
            this.buttonCheats2 = new Superfarmer.ButtonCheats();
            this.buttonCheats1 = new Superfarmer.ButtonCheats();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pbLanguageCzech)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLanguageEnglish)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(72, 116);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(31, 17);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.Text = "1";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.RadioButtons_CheckedChanged);
            // 
            // labelTitle
            // 
            this.labelTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTitle.Font = new System.Drawing.Font("Segoe UI", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTitle.Location = new System.Drawing.Point(12, 9);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(351, 66);
            this.labelTitle.TabIndex = 3;
            this.labelTitle.Text = "- SUPERFARMER -";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // linkLabelWiki
            // 
            this.linkLabelWiki.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.linkLabelWiki.Location = new System.Drawing.Point(81, 144);
            this.linkLabelWiki.Name = "linkLabelWiki";
            this.linkLabelWiki.Size = new System.Drawing.Size(189, 23);
            this.linkLabelWiki.TabIndex = 4;
            this.linkLabelWiki.TabStop = true;
            this.linkLabelWiki.Text = "Wikipedia";
            this.linkLabelWiki.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkLabelWiki.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelWiki_LinkClicked);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(11, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(335, 27);
            this.label1.TabIndex = 5;
            this.label1.Text = "Choose number of players:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(195, 115);
            this.textBox1.MaxLength = 20;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 6;
            this.textBox1.Text = "Player 1";
            // 
            // labelPlayer1
            // 
            this.labelPlayer1.AutoSize = true;
            this.labelPlayer1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPlayer1.Location = new System.Drawing.Point(109, 118);
            this.labelPlayer1.Name = "labelPlayer1";
            this.labelPlayer1.Size = new System.Drawing.Size(80, 13);
            this.labelPlayer1.TabIndex = 7;
            this.labelPlayer1.Text = "Player name:";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Checked = true;
            this.radioButton2.Location = new System.Drawing.Point(72, 139);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(31, 17);
            this.radioButton2.TabIndex = 0;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "2";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.RadioButtons_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(72, 162);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(31, 17);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.Text = "3";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.RadioButtons_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(72, 185);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(31, 17);
            this.radioButton4.TabIndex = 3;
            this.radioButton4.Text = "4";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.RadioButtons_CheckedChanged);
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(72, 208);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(31, 17);
            this.radioButton5.TabIndex = 4;
            this.radioButton5.Text = "5";
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.CheckedChanged += new System.EventHandler(this.RadioButtons_CheckedChanged);
            // 
            // labelPlayer2
            // 
            this.labelPlayer2.AutoSize = true;
            this.labelPlayer2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPlayer2.Location = new System.Drawing.Point(109, 141);
            this.labelPlayer2.Name = "labelPlayer2";
            this.labelPlayer2.Size = new System.Drawing.Size(80, 13);
            this.labelPlayer2.TabIndex = 13;
            this.labelPlayer2.Text = "Player name:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(195, 138);
            this.textBox2.MaxLength = 20;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 12;
            this.textBox2.Text = "Player 2";
            // 
            // labelPlayer3
            // 
            this.labelPlayer3.AutoSize = true;
            this.labelPlayer3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPlayer3.Location = new System.Drawing.Point(109, 164);
            this.labelPlayer3.Name = "labelPlayer3";
            this.labelPlayer3.Size = new System.Drawing.Size(80, 13);
            this.labelPlayer3.TabIndex = 15;
            this.labelPlayer3.Text = "Player name:";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(195, 161);
            this.textBox3.MaxLength = 20;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 14;
            this.textBox3.Text = "Player 3";
            // 
            // labelPlayer4
            // 
            this.labelPlayer4.AutoSize = true;
            this.labelPlayer4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPlayer4.Location = new System.Drawing.Point(109, 187);
            this.labelPlayer4.Name = "labelPlayer4";
            this.labelPlayer4.Size = new System.Drawing.Size(80, 13);
            this.labelPlayer4.TabIndex = 17;
            this.labelPlayer4.Text = "Player name:";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(195, 184);
            this.textBox4.MaxLength = 20;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 16;
            this.textBox4.Text = "Player 4";
            // 
            // labelPlayer5
            // 
            this.labelPlayer5.AutoSize = true;
            this.labelPlayer5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPlayer5.Location = new System.Drawing.Point(109, 210);
            this.labelPlayer5.Name = "labelPlayer5";
            this.labelPlayer5.Size = new System.Drawing.Size(80, 13);
            this.labelPlayer5.TabIndex = 19;
            this.labelPlayer5.Text = "Player name:";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(195, 207);
            this.textBox5.MaxLength = 20;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 18;
            this.textBox5.Text = "Player 5";
            // 
            // pbLanguageCzech
            // 
            this.pbLanguageCzech.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pbLanguageCzech.BackColor = System.Drawing.Color.White;
            this.pbLanguageCzech.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbLanguageCzech.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbLanguageCzech.Image = global::Superfarmer.Properties.Resources.flag_czech_gray;
            this.pbLanguageCzech.Location = new System.Drawing.Point(97, 91);
            this.pbLanguageCzech.Name = "pbLanguageCzech";
            this.pbLanguageCzech.Size = new System.Drawing.Size(75, 50);
            this.pbLanguageCzech.TabIndex = 105;
            this.pbLanguageCzech.TabStop = false;
            this.pbLanguageCzech.Click += new System.EventHandler(this.PBLanguageCzechClick);
            // 
            // pbLanguageEnglish
            // 
            this.pbLanguageEnglish.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pbLanguageEnglish.BackColor = System.Drawing.Color.White;
            this.pbLanguageEnglish.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbLanguageEnglish.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbLanguageEnglish.Image = global::Superfarmer.Properties.Resources.flag_english_gray;
            this.pbLanguageEnglish.Location = new System.Drawing.Point(178, 91);
            this.pbLanguageEnglish.Name = "pbLanguageEnglish";
            this.pbLanguageEnglish.Size = new System.Drawing.Size(75, 50);
            this.pbLanguageEnglish.TabIndex = 106;
            this.pbLanguageEnglish.TabStop = false;
            this.pbLanguageEnglish.Click += new System.EventHandler(this.PBLanguageEnglishClick);
            // 
            // buttonRules
            // 
            this.buttonRules.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonRules.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRules.Location = new System.Drawing.Point(0, 139);
            this.buttonRules.Name = "buttonRules";
            this.buttonRules.Size = new System.Drawing.Size(75, 28);
            this.buttonRules.TabIndex = 107;
            this.buttonRules.Text = "Rules";
            this.buttonRules.UseVisualStyleBackColor = true;
            this.buttonRules.Click += new System.EventHandler(this.ButtonRules_Click);
            // 
            // buttonPlay
            // 
            this.buttonPlay.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonPlay.BackColor = System.Drawing.SystemColors.Control;
            this.buttonPlay.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonPlay.Location = new System.Drawing.Point(75, 2);
            this.buttonPlay.Name = "buttonPlay";
            this.buttonPlay.Size = new System.Drawing.Size(200, 50);
            this.buttonPlay.TabIndex = 108;
            this.buttonPlay.Text = "Play";
            this.buttonPlay.UseVisualStyleBackColor = false;
            this.buttonPlay.Click += new System.EventHandler(this.ButtonPlay_Click);
            // 
            // buttonQuit
            // 
            this.buttonQuit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonQuit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonQuit.Location = new System.Drawing.Point(276, 139);
            this.buttonQuit.Name = "buttonQuit";
            this.buttonQuit.Size = new System.Drawing.Size(75, 28);
            this.buttonQuit.TabIndex = 109;
            this.buttonQuit.Text = "quit";
            this.buttonQuit.UseVisualStyleBackColor = true;
            this.buttonQuit.Click += new System.EventHandler(this.ButtonQuit_Click);
            // 
            // labelCheatAlert5
            // 
            this.labelCheatAlert5.AutoSize = true;
            this.labelCheatAlert5.BackColor = System.Drawing.Color.Transparent;
            this.labelCheatAlert5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCheatAlert5.ForeColor = System.Drawing.Color.Red;
            this.labelCheatAlert5.Location = new System.Drawing.Point(325, 207);
            this.labelCheatAlert5.Name = "labelCheatAlert5";
            this.labelCheatAlert5.Size = new System.Drawing.Size(14, 20);
            this.labelCheatAlert5.TabIndex = 119;
            this.labelCheatAlert5.Text = "!";
            this.labelCheatAlert5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelCheatAlert5.Visible = false;
            // 
            // labelCheatAlert4
            // 
            this.labelCheatAlert4.AutoSize = true;
            this.labelCheatAlert4.BackColor = System.Drawing.Color.Transparent;
            this.labelCheatAlert4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCheatAlert4.ForeColor = System.Drawing.Color.Red;
            this.labelCheatAlert4.Location = new System.Drawing.Point(325, 184);
            this.labelCheatAlert4.Name = "labelCheatAlert4";
            this.labelCheatAlert4.Size = new System.Drawing.Size(14, 20);
            this.labelCheatAlert4.TabIndex = 118;
            this.labelCheatAlert4.Text = "!";
            this.labelCheatAlert4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelCheatAlert4.Visible = false;
            // 
            // labelCheatAlert3
            // 
            this.labelCheatAlert3.AutoSize = true;
            this.labelCheatAlert3.BackColor = System.Drawing.Color.Transparent;
            this.labelCheatAlert3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCheatAlert3.ForeColor = System.Drawing.Color.Red;
            this.labelCheatAlert3.Location = new System.Drawing.Point(325, 162);
            this.labelCheatAlert3.Name = "labelCheatAlert3";
            this.labelCheatAlert3.Size = new System.Drawing.Size(14, 20);
            this.labelCheatAlert3.TabIndex = 117;
            this.labelCheatAlert3.Text = "!";
            this.labelCheatAlert3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelCheatAlert3.Visible = false;
            // 
            // labelCheatAlert2
            // 
            this.labelCheatAlert2.AutoSize = true;
            this.labelCheatAlert2.BackColor = System.Drawing.Color.Transparent;
            this.labelCheatAlert2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCheatAlert2.ForeColor = System.Drawing.Color.Red;
            this.labelCheatAlert2.Location = new System.Drawing.Point(325, 138);
            this.labelCheatAlert2.Name = "labelCheatAlert2";
            this.labelCheatAlert2.Size = new System.Drawing.Size(14, 20);
            this.labelCheatAlert2.TabIndex = 116;
            this.labelCheatAlert2.Text = "!";
            this.labelCheatAlert2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelCheatAlert2.Visible = false;
            // 
            // labelCheatAlert1
            // 
            this.labelCheatAlert1.AutoSize = true;
            this.labelCheatAlert1.BackColor = System.Drawing.Color.Transparent;
            this.labelCheatAlert1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCheatAlert1.ForeColor = System.Drawing.Color.Red;
            this.labelCheatAlert1.Location = new System.Drawing.Point(325, 115);
            this.labelCheatAlert1.Name = "labelCheatAlert1";
            this.labelCheatAlert1.Size = new System.Drawing.Size(14, 20);
            this.labelCheatAlert1.TabIndex = 115;
            this.labelCheatAlert1.Text = "!";
            this.labelCheatAlert1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelCheatAlert1.Visible = false;
            // 
            // buttonCheats5
            // 
            this.buttonCheats5.alertLabel = this.labelCheatAlert5;
            this.buttonCheats5.Location = new System.Drawing.Point(301, 207);
            this.buttonCheats5.Name = "buttonCheats5";
            this.buttonCheats5.PlayerNumber = 5;
            this.buttonCheats5.Size = new System.Drawing.Size(25, 20);
            this.buttonCheats5.TabIndex = 114;
            this.buttonCheats5.Text = "...";
            this.buttonCheats5.UseVisualStyleBackColor = true;
            this.buttonCheats5.Click += new System.EventHandler(this.ButtonCheats_Click);
            // 
            // buttonCheats4
            // 
            this.buttonCheats4.alertLabel = this.labelCheatAlert4;
            this.buttonCheats4.Location = new System.Drawing.Point(301, 184);
            this.buttonCheats4.Name = "buttonCheats4";
            this.buttonCheats4.PlayerNumber = 4;
            this.buttonCheats4.Size = new System.Drawing.Size(25, 20);
            this.buttonCheats4.TabIndex = 113;
            this.buttonCheats4.Text = "...";
            this.buttonCheats4.UseVisualStyleBackColor = true;
            this.buttonCheats4.Click += new System.EventHandler(this.ButtonCheats_Click);
            // 
            // buttonCheats3
            // 
            this.buttonCheats3.alertLabel = this.labelCheatAlert3;
            this.buttonCheats3.Location = new System.Drawing.Point(301, 161);
            this.buttonCheats3.Name = "buttonCheats3";
            this.buttonCheats3.PlayerNumber = 3;
            this.buttonCheats3.Size = new System.Drawing.Size(25, 20);
            this.buttonCheats3.TabIndex = 112;
            this.buttonCheats3.Text = "...";
            this.buttonCheats3.UseVisualStyleBackColor = true;
            this.buttonCheats3.Click += new System.EventHandler(this.ButtonCheats_Click);
            // 
            // buttonCheats2
            // 
            this.buttonCheats2.alertLabel = this.labelCheatAlert2;
            this.buttonCheats2.Location = new System.Drawing.Point(301, 138);
            this.buttonCheats2.Name = "buttonCheats2";
            this.buttonCheats2.PlayerNumber = 2;
            this.buttonCheats2.Size = new System.Drawing.Size(25, 20);
            this.buttonCheats2.TabIndex = 111;
            this.buttonCheats2.Text = "...";
            this.buttonCheats2.UseVisualStyleBackColor = true;
            this.buttonCheats2.Click += new System.EventHandler(this.ButtonCheats_Click);
            // 
            // buttonCheats1
            // 
            this.buttonCheats1.alertLabel = this.labelCheatAlert1;
            this.buttonCheats1.Location = new System.Drawing.Point(301, 115);
            this.buttonCheats1.Name = "buttonCheats1";
            this.buttonCheats1.PlayerNumber = 1;
            this.buttonCheats1.Size = new System.Drawing.Size(25, 20);
            this.buttonCheats1.TabIndex = 110;
            this.buttonCheats1.Text = "...";
            this.buttonCheats1.UseVisualStyleBackColor = true;
            this.buttonCheats1.Click += new System.EventHandler(this.ButtonCheats_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonRules);
            this.panel1.Controls.Add(this.buttonQuit);
            this.panel1.Controls.Add(this.pbLanguageCzech);
            this.panel1.Controls.Add(this.linkLabelWiki);
            this.panel1.Controls.Add(this.pbLanguageEnglish);
            this.panel1.Controls.Add(this.buttonPlay);
            this.panel1.Location = new System.Drawing.Point(12, 263);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(351, 167);
            this.panel1.TabIndex = 120;
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(375, 442);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.labelCheatAlert5);
            this.Controls.Add(this.labelCheatAlert4);
            this.Controls.Add(this.labelCheatAlert3);
            this.Controls.Add(this.labelCheatAlert2);
            this.Controls.Add(this.labelCheatAlert1);
            this.Controls.Add(this.buttonCheats5);
            this.Controls.Add(this.buttonCheats4);
            this.Controls.Add(this.buttonCheats3);
            this.Controls.Add(this.buttonCheats2);
            this.Controls.Add(this.buttonCheats1);
            this.Controls.Add(this.labelPlayer5);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.labelPlayer4);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.labelPlayer3);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.labelPlayer2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.radioButton5);
            this.Controls.Add(this.radioButton4);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.labelPlayer1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.radioButton1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(391, 481);
            this.MinimumSize = new System.Drawing.Size(391, 481);
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Superfarmer";
            ((System.ComponentModel.ISupportInitialize)(this.pbLanguageCzech)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLanguageEnglish)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.LinkLabel linkLabelWiki;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label labelPlayer1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.Label labelPlayer2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label labelPlayer3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label labelPlayer4;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label labelPlayer5;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.PictureBox pbLanguageCzech;
        private System.Windows.Forms.PictureBox pbLanguageEnglish;
        private System.Windows.Forms.Button buttonRules;
        private System.Windows.Forms.Button buttonPlay;
        private System.Windows.Forms.Button buttonQuit;
        private Superfarmer.ButtonCheats buttonCheats1;
        private Superfarmer.ButtonCheats buttonCheats2;
        private Superfarmer.ButtonCheats buttonCheats3;
        private Superfarmer.ButtonCheats buttonCheats4;
        private Superfarmer.ButtonCheats buttonCheats5;
        private System.Windows.Forms.Label labelCheatAlert1;
        private System.Windows.Forms.Label labelCheatAlert2;
        private System.Windows.Forms.Label labelCheatAlert3;
        private System.Windows.Forms.Label labelCheatAlert4;
        private System.Windows.Forms.Label labelCheatAlert5;
        private System.Windows.Forms.Panel panel1;
    }
}