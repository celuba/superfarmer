﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Superfarmer
{
    public partial class Cheats : Form
    {
        public int Rabbits { get; set; }
        public int Sheep { get; set; }
        public int Pigs { get; set; }
        public int Cows { get; set; }
        public int Horses { get; set; }
        public bool SmallDog { get; set; }
        public bool BigDog { get; set; }
        public bool Changed { get; set; }
        public Cheats(Cheats previouslySet, int playerNumber)
        {
            InitializeComponent();

            this.Text = Properties.Strings.cheatsTitle;
            labelHeader.Text = Properties.Strings.cheatsHeader + Convert.ToString(playerNumber);
            labelRabbit.Text = Properties.Strings.animalRabbit;
            labelSheep.Text = Properties.Strings.animalSheep;
            labelPig.Text = Properties.Strings.animalPig;
            labelCow.Text = Properties.Strings.animalCow;
            labelHorse.Text = Properties.Strings.animalHorse;
            labelSmallDog.Text = Properties.Strings.animalSmallDog;
            labelBigDog.Text = Properties.Strings.animalBigDog;
            labelWarning.Text = Properties.Strings.cheatsWarning;
            buttonSave.Text = Properties.Strings.cheatsSave;
            buttonDiscard.Text = Properties.Strings.cheatsDiscard;

            Rabbits = previouslySet != null ? previouslySet.Rabbits : 0;
            nudRabbits.Value = Rabbits;
            Sheep = previouslySet != null ? previouslySet.Sheep : 0;
            nudSheep.Value = Sheep;
            Pigs = previouslySet != null ? previouslySet.Pigs : 0;
            nudPigs.Value = Pigs;
            Cows = previouslySet != null ? previouslySet.Cows : 0;
            nudCows.Value = Cows;
            Horses = previouslySet != null ? previouslySet.Horses : 0;
            nudHorses.Value = Horses;
            SmallDog = previouslySet != null ? previouslySet.SmallDog : false;
            checkBoxSmallDog.Checked = SmallDog;
            BigDog = previouslySet != null ? previouslySet.BigDog : false;
            checkBoxBigDog.Checked = BigDog;
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            Rabbits = (int)nudRabbits.Value;
            Sheep = (int)nudSheep.Value;
            Pigs = (int)nudPigs.Value;
            Cows = (int)nudCows.Value;
            Horses = (int)nudHorses.Value;
            SmallDog = checkBoxSmallDog.Checked;
            BigDog = checkBoxBigDog.Checked;
            Changed = Rabbits != 0 || Sheep != 0 || Pigs != 0 || Cows != 0 || Horses != 0 || SmallDog || BigDog;
            this.DialogResult = Changed ? DialogResult.OK : DialogResult.Cancel;
            this.Close();
        }

        private void ButtonDiscard_Click(object sender, EventArgs e)
        {
            Changed = false;
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
