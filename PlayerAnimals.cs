﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Superfarmer
{
    public partial class PlayerAnimals : UserControl
    {
        public int maxNumber = 999;

        private int rabbit;
        private int sheep;
        private int pig;
        private int cow;
        private int horse;
        private bool smallDog;
        private bool bigDog;

        public PlayerAnimals()
        {
            InitializeComponent();
        }

        public Label PlayerName
        {
            get { return labelName; }
            set { labelName = value; }
        }

        public void PlayerNameColors(Color foreColor, Color backColor)
        {
            labelName.ForeColor = foreColor;
            labelName.BackColor = backColor;
        }
        public int Rabbit
        {
            get { return rabbit; }
            set
            {
                rabbit = value > maxNumber ? maxNumber : value;
                labelRabbit.Text = Convert.ToString(rabbit);
            }
        }
        public int Sheep
        {
            get { return sheep; }
            set
            {
                sheep = value > maxNumber ? maxNumber : value;
                labelSheep.Text = Convert.ToString(sheep);
            }
        }
        public int Pig
        {
            get { return pig; }
            set
            {
                pig = value > maxNumber ? maxNumber : value;
                labelPig.Text = Convert.ToString(pig);
            }
        }
        public int Cow
        {
            get { return cow; }
            set
            {
                cow = value > maxNumber ? maxNumber : value;
                labelCow.Text = Convert.ToString(cow);
            }
        }
        public int Horse
        {
            get { return horse; }
            set
            {
                horse = value > maxNumber ? maxNumber : value;
                labelHorse.Text = Convert.ToString(horse);
            }
        }
        public bool SmallDog
        {
            get { return smallDog; }
            set
            {
                smallDog = value;
                labelSmallDog.Text = smallDog ? "✔" : "✖";
            }
        }
        public bool BigDog
        {
            get { return bigDog; }
            set
            {
                bigDog = value;
                labelBigDog.Text = bigDog ? "✔" : "✖";
            }
        }

        public void SetByID(int id, int value)
        {
            switch (id)
            {
                case 1:
                    Rabbit = value;
                    break;
                case 2:
                    Sheep = value;
                    break;
                case 3:
                    Pig = value;
                    break;
                case 4:
                    Cow = value;
                    break;
                case 5:
                    Horse = value;
                    break;
            }
        }
        public int GetByID(int id)
        {
            switch (id)
            {
                case 1:
                    return Rabbit;
                case 2:
                    return Sheep;
                case 3:
                    return Pig;
                case 4:
                    return Cow;
                case 5:
                    return Horse;
            }
            return 0;
        }
    }
}
