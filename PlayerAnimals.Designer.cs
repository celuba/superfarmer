﻿namespace Superfarmer
{
    partial class PlayerAnimals
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelName = new System.Windows.Forms.Label();
            this.pbSmalldog = new System.Windows.Forms.PictureBox();
            this.pbHorse = new System.Windows.Forms.PictureBox();
            this.pbCow = new System.Windows.Forms.PictureBox();
            this.pbPig = new System.Windows.Forms.PictureBox();
            this.pbSheep = new System.Windows.Forms.PictureBox();
            this.pbRabbit = new System.Windows.Forms.PictureBox();
            this.pbBigdog = new System.Windows.Forms.PictureBox();
            this.labelSheep = new System.Windows.Forms.Label();
            this.labelPig = new System.Windows.Forms.Label();
            this.labelCow = new System.Windows.Forms.Label();
            this.labelRabbit = new System.Windows.Forms.Label();
            this.labelHorse = new System.Windows.Forms.Label();
            this.labelBigDog = new System.Windows.Forms.Label();
            this.labelSmallDog = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pbSmalldog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHorse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSheep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRabbit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBigdog)).BeginInit();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.Font = new System.Drawing.Font("Segoe UI Black", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.ForeColor = System.Drawing.Color.Silver;
            this.labelName.Location = new System.Drawing.Point(3, 0);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(200, 67);
            this.labelName.TabIndex = 96;
            this.labelName.Text = "name";
            this.labelName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pbSmalldog
            // 
            this.pbSmalldog.BackColor = System.Drawing.Color.White;
            this.pbSmalldog.Image = global::Superfarmer.Properties.Resources.small_smalldog;
            this.pbSmalldog.ImageLocation = "";
            this.pbSmalldog.Location = new System.Drawing.Point(6, 408);
            this.pbSmalldog.Name = "pbSmalldog";
            this.pbSmalldog.Size = new System.Drawing.Size(61, 46);
            this.pbSmalldog.TabIndex = 97;
            this.pbSmalldog.TabStop = false;
            // 
            // pbHorse
            // 
            this.pbHorse.BackColor = System.Drawing.Color.White;
            this.pbHorse.Image = global::Superfarmer.Properties.Resources.small_horse;
            this.pbHorse.Location = new System.Drawing.Point(5, 295);
            this.pbHorse.Name = "pbHorse";
            this.pbHorse.Size = new System.Drawing.Size(61, 46);
            this.pbHorse.TabIndex = 98;
            this.pbHorse.TabStop = false;
            // 
            // pbCow
            // 
            this.pbCow.BackColor = System.Drawing.Color.White;
            this.pbCow.Image = global::Superfarmer.Properties.Resources.small_cow;
            this.pbCow.Location = new System.Drawing.Point(6, 243);
            this.pbCow.Name = "pbCow";
            this.pbCow.Size = new System.Drawing.Size(61, 46);
            this.pbCow.TabIndex = 99;
            this.pbCow.TabStop = false;
            // 
            // pbPig
            // 
            this.pbPig.BackColor = System.Drawing.Color.White;
            this.pbPig.Image = global::Superfarmer.Properties.Resources.small_pig;
            this.pbPig.Location = new System.Drawing.Point(6, 191);
            this.pbPig.Name = "pbPig";
            this.pbPig.Size = new System.Drawing.Size(61, 46);
            this.pbPig.TabIndex = 100;
            this.pbPig.TabStop = false;
            // 
            // pbSheep
            // 
            this.pbSheep.BackColor = System.Drawing.Color.White;
            this.pbSheep.Image = global::Superfarmer.Properties.Resources.small_sheep;
            this.pbSheep.Location = new System.Drawing.Point(6, 139);
            this.pbSheep.Name = "pbSheep";
            this.pbSheep.Size = new System.Drawing.Size(61, 46);
            this.pbSheep.TabIndex = 101;
            this.pbSheep.TabStop = false;
            // 
            // pbRabbit
            // 
            this.pbRabbit.BackColor = System.Drawing.Color.White;
            this.pbRabbit.Image = global::Superfarmer.Properties.Resources.small_rabbit;
            this.pbRabbit.Location = new System.Drawing.Point(6, 87);
            this.pbRabbit.Name = "pbRabbit";
            this.pbRabbit.Size = new System.Drawing.Size(61, 46);
            this.pbRabbit.TabIndex = 102;
            this.pbRabbit.TabStop = false;
            // 
            // pbBigdog
            // 
            this.pbBigdog.BackColor = System.Drawing.Color.White;
            this.pbBigdog.Image = global::Superfarmer.Properties.Resources.small_bigdog;
            this.pbBigdog.Location = new System.Drawing.Point(6, 460);
            this.pbBigdog.Name = "pbBigdog";
            this.pbBigdog.Size = new System.Drawing.Size(61, 46);
            this.pbBigdog.TabIndex = 103;
            this.pbBigdog.TabStop = false;
            // 
            // labelSheep
            // 
            this.labelSheep.Font = new System.Drawing.Font("Verdana", 26.25F, System.Drawing.FontStyle.Bold);
            this.labelSheep.Location = new System.Drawing.Point(90, 139);
            this.labelSheep.Name = "labelSheep";
            this.labelSheep.Size = new System.Drawing.Size(113, 46);
            this.labelSheep.TabIndex = 104;
            this.labelSheep.Tag = "";
            this.labelSheep.Text = "x";
            // 
            // labelPig
            // 
            this.labelPig.Font = new System.Drawing.Font("Verdana", 26.25F, System.Drawing.FontStyle.Bold);
            this.labelPig.Location = new System.Drawing.Point(90, 191);
            this.labelPig.Name = "labelPig";
            this.labelPig.Size = new System.Drawing.Size(113, 46);
            this.labelPig.TabIndex = 105;
            this.labelPig.Tag = "";
            this.labelPig.Text = "x";
            // 
            // labelCow
            // 
            this.labelCow.Font = new System.Drawing.Font("Verdana", 26.25F, System.Drawing.FontStyle.Bold);
            this.labelCow.Location = new System.Drawing.Point(90, 243);
            this.labelCow.Name = "labelCow";
            this.labelCow.Size = new System.Drawing.Size(113, 46);
            this.labelCow.TabIndex = 106;
            this.labelCow.Tag = "";
            this.labelCow.Text = "x";
            // 
            // labelRabbit
            // 
            this.labelRabbit.Font = new System.Drawing.Font("Verdana", 26.25F, System.Drawing.FontStyle.Bold);
            this.labelRabbit.Location = new System.Drawing.Point(90, 93);
            this.labelRabbit.Name = "labelRabbit";
            this.labelRabbit.Size = new System.Drawing.Size(113, 46);
            this.labelRabbit.TabIndex = 107;
            this.labelRabbit.Tag = "";
            this.labelRabbit.Text = "x";
            // 
            // labelHorse
            // 
            this.labelHorse.Font = new System.Drawing.Font("Verdana", 26.25F, System.Drawing.FontStyle.Bold);
            this.labelHorse.Location = new System.Drawing.Point(90, 295);
            this.labelHorse.Name = "labelHorse";
            this.labelHorse.Size = new System.Drawing.Size(113, 46);
            this.labelHorse.TabIndex = 108;
            this.labelHorse.Tag = "";
            this.labelHorse.Text = "x";
            // 
            // labelBigDog
            // 
            this.labelBigDog.Font = new System.Drawing.Font("Verdana", 26.25F, System.Drawing.FontStyle.Bold);
            this.labelBigDog.Location = new System.Drawing.Point(90, 460);
            this.labelBigDog.Name = "labelBigDog";
            this.labelBigDog.Size = new System.Drawing.Size(113, 46);
            this.labelBigDog.TabIndex = 109;
            this.labelBigDog.Text = "x";
            // 
            // labelSmallDog
            // 
            this.labelSmallDog.Font = new System.Drawing.Font("Verdana", 26.25F, System.Drawing.FontStyle.Bold);
            this.labelSmallDog.Location = new System.Drawing.Point(90, 408);
            this.labelSmallDog.Name = "labelSmallDog";
            this.labelSmallDog.Size = new System.Drawing.Size(113, 46);
            this.labelSmallDog.TabIndex = 110;
            this.labelSmallDog.Text = "x";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Silver;
            this.panel5.Location = new System.Drawing.Point(3, 371);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(200, 4);
            this.panel5.TabIndex = 95;
            // 
            // PlayerAnimals
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.labelSmallDog);
            this.Controls.Add(this.labelBigDog);
            this.Controls.Add(this.labelHorse);
            this.Controls.Add(this.labelRabbit);
            this.Controls.Add(this.labelCow);
            this.Controls.Add(this.labelPig);
            this.Controls.Add(this.labelSheep);
            this.Controls.Add(this.pbBigdog);
            this.Controls.Add(this.pbRabbit);
            this.Controls.Add(this.pbSheep);
            this.Controls.Add(this.pbPig);
            this.Controls.Add(this.pbCow);
            this.Controls.Add(this.pbHorse);
            this.Controls.Add(this.pbSmalldog);
            this.Controls.Add(this.labelName);
            this.Name = "PlayerAnimals";
            this.Size = new System.Drawing.Size(206, 509);
            ((System.ComponentModel.ISupportInitialize)(this.pbSmalldog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHorse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSheep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRabbit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBigdog)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.PictureBox pbSmalldog;
        private System.Windows.Forms.PictureBox pbHorse;
        private System.Windows.Forms.PictureBox pbCow;
        private System.Windows.Forms.PictureBox pbPig;
        private System.Windows.Forms.PictureBox pbSheep;
        private System.Windows.Forms.PictureBox pbRabbit;
        private System.Windows.Forms.PictureBox pbBigdog;
        private System.Windows.Forms.Label labelSheep;
        private System.Windows.Forms.Label labelPig;
        private System.Windows.Forms.Label labelCow;
        private System.Windows.Forms.Label labelRabbit;
        private System.Windows.Forms.Label labelHorse;
        private System.Windows.Forms.Label labelBigDog;
        private System.Windows.Forms.Label labelSmallDog;
        private System.Windows.Forms.Panel panel5;
    }
}
