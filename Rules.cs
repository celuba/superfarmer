﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Superfarmer
{

	public partial class Rules : Form
	{
		public Rules()
		{
			InitializeComponent();
			
            this.Text = Properties.Strings.rulesTitle;
            labelHeader.Text = Properties.Strings.rulesHeader;
            label1.Text = Properties.Strings.rules1;
            label2.Text = Properties.Strings.rules2;
            label3.Text = Properties.Strings.rules3;
            label4.Text = Properties.Strings.rules4;
            label5.Text = Properties.Strings.rules5;
            label6.Text = Properties.Strings.rules6;
            label7.Text = Properties.Strings.rules7;
            label8.Text = Properties.Strings.rules8;
            label9.Text = Properties.Strings.rules9;
            label10.Text = Properties.Strings.rules10;
            label11.Text = Properties.Strings.rules11;
            label12.Text = Properties.Strings.rules12;
            label13.Text = Properties.Strings.rules13;
        }
		
		//zavření okna s nápovědou
		void buttonCloseClick(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
