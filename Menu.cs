﻿using System;
using System.Windows.Forms;
using System.Threading;
using System.Globalization;
using System.IO;

namespace Superfarmer
{
    public partial class Menu : Form
    {
        int numberOfPlayers = 2;
        Cheats[] startingAnimals = new Cheats[5];
        string englishLocation = @"en\Superfarmer.resources.dll";
        bool englishAvailable;

        public Menu()
        {
            InitializeComponent();

            for (int i = 0; i < startingAnimals.Length; i++)
                startingAnimals[i] = new Cheats(null, 0);

            radioButton2.Checked = true;

            SetupMenu();
        }

        void SetupMenu()
        {
            this.Text = Properties.Strings.applicationTitle;
            labelTitle.Text = "- " + Properties.Strings.applicationTitle.ToUpper() + " -";
            label1.Text = Properties.Strings.menuChoosePlayers;
            labelPlayer1.Text = Properties.Strings.menuPlayerName;
            labelPlayer2.Text = Properties.Strings.menuPlayerName;
            labelPlayer3.Text = Properties.Strings.menuPlayerName;
            labelPlayer4.Text = Properties.Strings.menuPlayerName;
            labelPlayer5.Text = Properties.Strings.menuPlayerName;
            textBox1.Text = Properties.Strings.menuPlayer1;
            textBox2.Text = Properties.Strings.menuPlayer2;
            textBox3.Text = Properties.Strings.menuPlayer3;
            textBox4.Text = Properties.Strings.menuPlayer4;
            textBox5.Text = Properties.Strings.menuPlayer5;
            buttonPlay.Text = Properties.Strings.menuButtonPlay;
            buttonRules.Text = Properties.Strings.menuButtonRules;
            buttonQuit.Text = Properties.Strings.buttonQuit;
            englishAvailable = File.Exists(englishLocation);
            if (Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName == "en" && englishAvailable)
                pbLanguageEnglish.Image = Properties.Resources.flag_english;
            else
                pbLanguageCzech.Image = Properties.Resources.flag_czech;
            linkLabelWiki.Text = Properties.Strings.menuWikiLinkText;

            UpdatePlayerFields();
        }

        void UpdatePlayerFields()
        {
            labelPlayer2.Visible = false;
            textBox2.Visible = false;
            buttonCheats2.Visible = false;
            labelCheatAlert2.Visible = false;
            labelPlayer3.Visible = false;
            textBox3.Visible = false;
            buttonCheats3.Visible = false;
            labelCheatAlert3.Visible = false;
            labelPlayer4.Visible = false;
            textBox4.Visible = false;
            buttonCheats4.Visible = false;
            labelCheatAlert4.Visible = false;
            labelPlayer5.Visible = false;
            textBox5.Visible = false;
            buttonCheats5.Visible = false;
            labelCheatAlert5.Visible = false;
            if (numberOfPlayers >= 2)
            {
                labelPlayer2.Visible = true;
                textBox2.Visible = true;
                buttonCheats2.Visible = true;
                labelCheatAlert2.Visible = startingAnimals[1].Changed ? true : false;
            }
            if (numberOfPlayers >= 3)
            {
                labelPlayer3.Visible = true;
                textBox3.Visible = true;
                buttonCheats3.Visible = true;
                labelCheatAlert3.Visible = startingAnimals[2].Changed ? true : false;
            }
            if (numberOfPlayers >= 4)
            {
                labelPlayer4.Visible = true;
                textBox4.Visible = true;
                buttonCheats4.Visible = true;
                labelCheatAlert4.Visible = startingAnimals[3].Changed ? true : false;
            }
            if (numberOfPlayers == 5)
            {
                labelPlayer5.Visible = true;
                textBox5.Visible = true;
                buttonCheats5.Visible = true;
                labelCheatAlert5.Visible = startingAnimals[4].Changed ? true : false;
            }
        }

        private void ButtonPlay_Click(object sender, EventArgs e)
        {
            string[] players = new string[55];
            players[0] = textBox1.Text;
            players[1] = textBox2.Text;
            players[2] = textBox3.Text;
            players[3] = textBox4.Text;
            players[4] = textBox5.Text;

            Board board = new Board(numberOfPlayers, players, startingAnimals);
            board.Show();
            this.Close();
        }

        #region Language
        void PBLanguageCzechClick(object sender, EventArgs e)
        {
            string language = "cs";
            if (Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName != language)
            {
                pbLanguageCzech.Image = Properties.Resources.flag_czech;
                pbLanguageEnglish.Image = Properties.Resources.flag_english_gray;
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(language);
                SetupMenu();
            }
        }
        void PBLanguageEnglishClick(object sender, EventArgs e)
        {
            string language = "en";
            if (englishAvailable)
            {
                if (Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName != language)
                {
                    pbLanguageCzech.Image = Properties.Resources.flag_czech_gray;
                    pbLanguageEnglish.Image = Properties.Resources.flag_english;
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(language);
                    SetupMenu();

                }
            }
            else
                MessageBox.Show(Properties.Strings.englishUnavailableText + englishLocation + ".", Properties.Strings.englishUnavailableTitle, MessageBoxButtons.OK);
        }
        #endregion

        private void LinkLabelWiki_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(Properties.Strings.menuWikiLink);
        }

        private void RadioButtons_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked) numberOfPlayers = 1;
            else if (radioButton2.Checked) numberOfPlayers = 2;
            else if (radioButton3.Checked) numberOfPlayers = 3;
            else if (radioButton4.Checked) numberOfPlayers = 4;
            else if (radioButton5.Checked) numberOfPlayers = 5;
            UpdatePlayerFields();
        }

        void ButtonQuit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ButtonRules_Click(object sender, EventArgs e)
        {
            Rules rules = new Rules();
            rules.Show();
        }

        private void ButtonCheats_Click(object sender, EventArgs e)
        {
            ButtonCheats clicked = (ButtonCheats)sender;
            using (Cheats cheats = new Cheats(startingAnimals[clicked.PlayerNumber - 1], clicked.PlayerNumber))
            {
                DialogResult result = cheats.ShowDialog();
                if (result == DialogResult.OK)
                {
                    startingAnimals[clicked.PlayerNumber - 1] = cheats;
                    clicked.alertLabel.Visible = true;
                }
                else
                {
                    startingAnimals[clicked.PlayerNumber - 1] = new Cheats(null, 0);
                    clicked.alertLabel.Visible = false;
                }
            }
        }
    }
}
