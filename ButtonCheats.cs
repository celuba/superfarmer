﻿using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;


namespace Superfarmer
{
    class ButtonCheats : Button
    {
        [Category("Values"),
         Browsable(true)]
        public int PlayerNumber { get; set; }
        [Category("Values"),
         Browsable(true)]
        public Label alertLabel { get; set; }
    }
}
