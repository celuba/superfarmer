﻿/*
 * Created by SharpDevelop.
 * User: filip
 * Date: 20.04.2016
 * Time: 21:41
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace Superfarmer
{
	partial class Board
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            this.buttonRules = new System.Windows.Forms.Button();
            this.buttonRestart = new System.Windows.Forms.Button();
            this.buttonQuit = new System.Windows.Forms.Button();
            this.panelMarket2 = new System.Windows.Forms.Panel();
            this.labelMarketC6 = new System.Windows.Forms.Label();
            this.labelMarketC2 = new System.Windows.Forms.Label();
            this.pbDice1 = new System.Windows.Forms.PictureBox();
            this.labelMarketC4 = new System.Windows.Forms.Label();
            this.labelMarketC3 = new System.Windows.Forms.Label();
            this.labelMarketC5 = new System.Windows.Forms.Label();
            this.pbDice2 = new System.Windows.Forms.PictureBox();
            this.labelDice1 = new System.Windows.Forms.Label();
            this.labelDice2 = new System.Windows.Forms.Label();
            this.buttonRoll = new System.Windows.Forms.Button();
            this.mpbMarketR2 = new Superfarmer.MarketPictureBox();
            this.mpbMarketL2 = new Superfarmer.MarketPictureBox();
            this.labelMarketC1 = new System.Windows.Forms.Label();
            this.mpbMarketR1 = new Superfarmer.MarketPictureBox();
            this.mpbMarketL1 = new Superfarmer.MarketPictureBox();
            this.mpbMarketR3 = new Superfarmer.MarketPictureBox();
            this.mpbMarketL3 = new Superfarmer.MarketPictureBox();
            this.labelMarketL1 = new System.Windows.Forms.Label();
            this.mpbMarketR4 = new Superfarmer.MarketPictureBox();
            this.mpbMarketL4 = new Superfarmer.MarketPictureBox();
            this.mpbMarketR5 = new Superfarmer.MarketPictureBox();
            this.mpbMarketL5 = new Superfarmer.MarketPictureBox();
            this.labelMarketR1 = new System.Windows.Forms.Label();
            this.mpbMarketL6 = new Superfarmer.MarketPictureBox();
            this.mpbMarketR6 = new Superfarmer.MarketPictureBox();
            this.labelMarketR6 = new System.Windows.Forms.Label();
            this.labelMarketL6 = new System.Windows.Forms.Label();
            this.labelMarketR5 = new System.Windows.Forms.Label();
            this.labelMarketL5 = new System.Windows.Forms.Label();
            this.labelMarketR4 = new System.Windows.Forms.Label();
            this.labelMarketL4 = new System.Windows.Forms.Label();
            this.labelMarketR3 = new System.Windows.Forms.Label();
            this.labelMarketL3 = new System.Windows.Forms.Label();
            this.labelMarketR2 = new System.Windows.Forms.Label();
            this.labelMarketL2 = new System.Windows.Forms.Label();
            this.panelMarket1 = new System.Windows.Forms.Panel();
            this.playerTable = new Superfarmer.PlayerAnimals();
            this.panelMarket2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDice1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDice2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketR2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketL2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketR1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketL1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketR3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketL3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketR4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketL4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketR5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketL5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketL6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketR6)).BeginInit();
            this.panelMarket1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRules
            // 
            this.buttonRules.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRules.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRules.Location = new System.Drawing.Point(12, 559);
            this.buttonRules.Name = "buttonRules";
            this.buttonRules.Size = new System.Drawing.Size(75, 28);
            this.buttonRules.TabIndex = 1;
            this.buttonRules.Text = "rules";
            this.buttonRules.UseVisualStyleBackColor = true;
            this.buttonRules.Click += new System.EventHandler(this.ButtonRulesClick);
            // 
            // buttonRestart
            // 
            this.buttonRestart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRestart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRestart.Location = new System.Drawing.Point(389, 559);
            this.buttonRestart.Name = "buttonRestart";
            this.buttonRestart.Size = new System.Drawing.Size(75, 28);
            this.buttonRestart.TabIndex = 3;
            this.buttonRestart.Text = "restart";
            this.buttonRestart.UseVisualStyleBackColor = true;
            this.buttonRestart.Click += new System.EventHandler(this.ButtonRestartClick);
            // 
            // buttonQuit
            // 
            this.buttonQuit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonQuit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonQuit.Location = new System.Drawing.Point(470, 559);
            this.buttonQuit.Name = "buttonQuit";
            this.buttonQuit.Size = new System.Drawing.Size(75, 28);
            this.buttonQuit.TabIndex = 2;
            this.buttonQuit.Text = "quit";
            this.buttonQuit.UseVisualStyleBackColor = true;
            this.buttonQuit.Click += new System.EventHandler(this.ButtonQuitClick);
            // 
            // panelMarket2
            // 
            this.panelMarket2.BackColor = System.Drawing.Color.White;
            this.panelMarket2.Controls.Add(this.labelMarketC6);
            this.panelMarket2.Controls.Add(this.labelMarketC2);
            this.panelMarket2.Controls.Add(this.pbDice1);
            this.panelMarket2.Controls.Add(this.labelMarketC4);
            this.panelMarket2.Controls.Add(this.labelMarketC3);
            this.panelMarket2.Controls.Add(this.labelMarketC5);
            this.panelMarket2.Controls.Add(this.pbDice2);
            this.panelMarket2.Controls.Add(this.labelDice1);
            this.panelMarket2.Controls.Add(this.labelDice2);
            this.panelMarket2.Controls.Add(this.buttonRoll);
            this.panelMarket2.Controls.Add(this.mpbMarketR2);
            this.panelMarket2.Controls.Add(this.labelMarketC1);
            this.panelMarket2.Controls.Add(this.mpbMarketR1);
            this.panelMarket2.Controls.Add(this.mpbMarketR3);
            this.panelMarket2.Controls.Add(this.labelMarketL1);
            this.panelMarket2.Controls.Add(this.mpbMarketR4);
            this.panelMarket2.Controls.Add(this.mpbMarketL1);
            this.panelMarket2.Controls.Add(this.mpbMarketR5);
            this.panelMarket2.Controls.Add(this.labelMarketR1);
            this.panelMarket2.Controls.Add(this.mpbMarketL5);
            this.panelMarket2.Controls.Add(this.mpbMarketL6);
            this.panelMarket2.Controls.Add(this.mpbMarketL4);
            this.panelMarket2.Controls.Add(this.mpbMarketR6);
            this.panelMarket2.Controls.Add(this.mpbMarketL3);
            this.panelMarket2.Controls.Add(this.mpbMarketL2);
            this.panelMarket2.Controls.Add(this.labelMarketR6);
            this.panelMarket2.Controls.Add(this.labelMarketL6);
            this.panelMarket2.Controls.Add(this.labelMarketR5);
            this.panelMarket2.Controls.Add(this.labelMarketL5);
            this.panelMarket2.Controls.Add(this.labelMarketR4);
            this.panelMarket2.Controls.Add(this.labelMarketL4);
            this.panelMarket2.Controls.Add(this.labelMarketR3);
            this.panelMarket2.Controls.Add(this.labelMarketL3);
            this.panelMarket2.Controls.Add(this.labelMarketR2);
            this.panelMarket2.Controls.Add(this.labelMarketL2);
            this.panelMarket2.Location = new System.Drawing.Point(9, 9);
            this.panelMarket2.Name = "panelMarket2";
            this.panelMarket2.Size = new System.Drawing.Size(304, 519);
            this.panelMarket2.TabIndex = 40;
            // 
            // labelMarketC6
            // 
            this.labelMarketC6.BackColor = System.Drawing.Color.White;
            this.labelMarketC6.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMarketC6.Location = new System.Drawing.Point(137, 466);
            this.labelMarketC6.Name = "labelMarketC6";
            this.labelMarketC6.Size = new System.Drawing.Size(34, 46);
            this.labelMarketC6.TabIndex = 58;
            this.labelMarketC6.Text = "=";
            // 
            // labelMarketC2
            // 
            this.labelMarketC2.BackColor = System.Drawing.Color.White;
            this.labelMarketC2.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMarketC2.Location = new System.Drawing.Point(137, 282);
            this.labelMarketC2.Name = "labelMarketC2";
            this.labelMarketC2.Size = new System.Drawing.Size(34, 46);
            this.labelMarketC2.TabIndex = 54;
            this.labelMarketC2.Text = "=";
            // 
            // pbDice1
            // 
            this.pbDice1.BackColor = System.Drawing.Color.White;
            this.pbDice1.Image = global::Superfarmer.Properties.Resources.big_dice;
            this.pbDice1.ImageLocation = "";
            this.pbDice1.InitialImage = null;
            this.pbDice1.Location = new System.Drawing.Point(7, 7);
            this.pbDice1.Name = "pbDice1";
            this.pbDice1.Size = new System.Drawing.Size(143, 107);
            this.pbDice1.TabIndex = 10;
            this.pbDice1.TabStop = false;
            // 
            // labelMarketC4
            // 
            this.labelMarketC4.BackColor = System.Drawing.Color.White;
            this.labelMarketC4.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMarketC4.Location = new System.Drawing.Point(137, 374);
            this.labelMarketC4.Name = "labelMarketC4";
            this.labelMarketC4.Size = new System.Drawing.Size(34, 46);
            this.labelMarketC4.TabIndex = 56;
            this.labelMarketC4.Text = "=";
            // 
            // labelMarketC3
            // 
            this.labelMarketC3.BackColor = System.Drawing.Color.White;
            this.labelMarketC3.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMarketC3.Location = new System.Drawing.Point(137, 328);
            this.labelMarketC3.Name = "labelMarketC3";
            this.labelMarketC3.Size = new System.Drawing.Size(34, 46);
            this.labelMarketC3.TabIndex = 55;
            this.labelMarketC3.Text = "=";
            // 
            // labelMarketC5
            // 
            this.labelMarketC5.BackColor = System.Drawing.Color.White;
            this.labelMarketC5.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMarketC5.Location = new System.Drawing.Point(137, 420);
            this.labelMarketC5.Name = "labelMarketC5";
            this.labelMarketC5.Size = new System.Drawing.Size(34, 46);
            this.labelMarketC5.TabIndex = 57;
            this.labelMarketC5.Text = "=";
            // 
            // pbDice2
            // 
            this.pbDice2.BackColor = System.Drawing.Color.White;
            this.pbDice2.Image = global::Superfarmer.Properties.Resources.big_dice;
            this.pbDice2.ImageLocation = "";
            this.pbDice2.Location = new System.Drawing.Point(154, 7);
            this.pbDice2.Name = "pbDice2";
            this.pbDice2.Size = new System.Drawing.Size(143, 107);
            this.pbDice2.TabIndex = 11;
            this.pbDice2.TabStop = false;
            // 
            // labelDice1
            // 
            this.labelDice1.BackColor = System.Drawing.Color.Silver;
            this.labelDice1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDice1.Location = new System.Drawing.Point(7, 117);
            this.labelDice1.Name = "labelDice1";
            this.labelDice1.Size = new System.Drawing.Size(143, 45);
            this.labelDice1.TabIndex = 6;
            this.labelDice1.Text = "- - -";
            this.labelDice1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDice2
            // 
            this.labelDice2.BackColor = System.Drawing.Color.Silver;
            this.labelDice2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDice2.Location = new System.Drawing.Point(154, 117);
            this.labelDice2.Name = "labelDice2";
            this.labelDice2.Size = new System.Drawing.Size(143, 45);
            this.labelDice2.TabIndex = 7;
            this.labelDice2.Text = "- - -";
            this.labelDice2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonRoll
            // 
            this.buttonRoll.BackColor = System.Drawing.SystemColors.Control;
            this.buttonRoll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonRoll.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRoll.Location = new System.Drawing.Point(7, 165);
            this.buttonRoll.Name = "buttonRoll";
            this.buttonRoll.Size = new System.Drawing.Size(290, 68);
            this.buttonRoll.TabIndex = 0;
            this.buttonRoll.Text = "throw";
            this.buttonRoll.UseVisualStyleBackColor = false;
            this.buttonRoll.Click += new System.EventHandler(this.ButtonRollClick);
            // 
            // mpbMarketR2
            // 
            this.mpbMarketR2.Active = false;
            this.mpbMarketR2.ActiveImage = global::Superfarmer.Properties.Resources.small_sheep;
            this.mpbMarketR2.AnimalQuantity = 2;
            this.mpbMarketR2.AnimalToSell = this.mpbMarketL2;
            this.mpbMarketR2.AnimalType = Superfarmer.MarketPictureBox.Animals.sheep;
            this.mpbMarketR2.BackColor = System.Drawing.Color.White;
            this.mpbMarketR2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mpbMarketR2.Dog = Superfarmer.MarketPictureBox.DogT.no;
            this.mpbMarketR2.Image = global::Superfarmer.Properties.Resources.small_sheep_gray;
            this.mpbMarketR2.InactiveImage = global::Superfarmer.Properties.Resources.small_sheep_gray;
            this.mpbMarketR2.Location = new System.Drawing.Point(212, 282);
            this.mpbMarketR2.Name = "mpbMarketR2";
            this.mpbMarketR2.Size = new System.Drawing.Size(61, 46);
            this.mpbMarketR2.TabIndex = 51;
            this.mpbMarketR2.TabStop = false;
            this.mpbMarketR2.Click += new System.EventHandler(this.PBMarketClick);
            // 
            // mpbMarketL2
            // 
            this.mpbMarketL2.Active = false;
            this.mpbMarketL2.ActiveImage = global::Superfarmer.Properties.Resources.small_pig;
            this.mpbMarketL2.AnimalQuantity = 1;
            this.mpbMarketL2.AnimalToSell = this.mpbMarketR2;
            this.mpbMarketL2.AnimalType = Superfarmer.MarketPictureBox.Animals.pig;
            this.mpbMarketL2.BackColor = System.Drawing.Color.White;
            this.mpbMarketL2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mpbMarketL2.Dog = Superfarmer.MarketPictureBox.DogT.no;
            this.mpbMarketL2.Image = global::Superfarmer.Properties.Resources.small_pig_gray;
            this.mpbMarketL2.ImageLocation = "";
            this.mpbMarketL2.InactiveImage = global::Superfarmer.Properties.Resources.small_pig_gray;
            this.mpbMarketL2.Location = new System.Drawing.Point(57, 282);
            this.mpbMarketL2.Name = "mpbMarketL2";
            this.mpbMarketL2.Size = new System.Drawing.Size(61, 46);
            this.mpbMarketL2.TabIndex = 20;
            this.mpbMarketL2.TabStop = false;
            this.mpbMarketL2.Click += new System.EventHandler(this.PBMarketClick);
            // 
            // labelMarketC1
            // 
            this.labelMarketC1.BackColor = System.Drawing.Color.White;
            this.labelMarketC1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMarketC1.Location = new System.Drawing.Point(137, 236);
            this.labelMarketC1.Name = "labelMarketC1";
            this.labelMarketC1.Size = new System.Drawing.Size(34, 46);
            this.labelMarketC1.TabIndex = 53;
            this.labelMarketC1.Text = "=";
            // 
            // mpbMarketR1
            // 
            this.mpbMarketR1.Active = false;
            this.mpbMarketR1.ActiveImage = global::Superfarmer.Properties.Resources.small_rabbit;
            this.mpbMarketR1.AnimalQuantity = 6;
            this.mpbMarketR1.AnimalToSell = this.mpbMarketL1;
            this.mpbMarketR1.AnimalType = Superfarmer.MarketPictureBox.Animals.rabbit;
            this.mpbMarketR1.BackColor = System.Drawing.Color.White;
            this.mpbMarketR1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mpbMarketR1.Dog = Superfarmer.MarketPictureBox.DogT.no;
            this.mpbMarketR1.Image = global::Superfarmer.Properties.Resources.small_rabbit_gray;
            this.mpbMarketR1.InactiveImage = global::Superfarmer.Properties.Resources.small_rabbit_gray;
            this.mpbMarketR1.Location = new System.Drawing.Point(212, 236);
            this.mpbMarketR1.Name = "mpbMarketR1";
            this.mpbMarketR1.Size = new System.Drawing.Size(61, 46);
            this.mpbMarketR1.TabIndex = 52;
            this.mpbMarketR1.TabStop = false;
            this.mpbMarketR1.Click += new System.EventHandler(this.PBMarketClick);
            // 
            // mpbMarketL1
            // 
            this.mpbMarketL1.Active = false;
            this.mpbMarketL1.ActiveImage = global::Superfarmer.Properties.Resources.small_sheep;
            this.mpbMarketL1.AnimalQuantity = 1;
            this.mpbMarketL1.AnimalToSell = this.mpbMarketR1;
            this.mpbMarketL1.AnimalType = Superfarmer.MarketPictureBox.Animals.sheep;
            this.mpbMarketL1.BackColor = System.Drawing.Color.White;
            this.mpbMarketL1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mpbMarketL1.Dog = Superfarmer.MarketPictureBox.DogT.no;
            this.mpbMarketL1.Image = global::Superfarmer.Properties.Resources.small_sheep_gray;
            this.mpbMarketL1.InactiveImage = global::Superfarmer.Properties.Resources.small_sheep_gray;
            this.mpbMarketL1.Location = new System.Drawing.Point(57, 236);
            this.mpbMarketL1.Name = "mpbMarketL1";
            this.mpbMarketL1.Size = new System.Drawing.Size(61, 46);
            this.mpbMarketL1.TabIndex = 42;
            this.mpbMarketL1.TabStop = false;
            this.mpbMarketL1.Click += new System.EventHandler(this.PBMarketClick);
            // 
            // mpbMarketR3
            // 
            this.mpbMarketR3.Active = false;
            this.mpbMarketR3.ActiveImage = global::Superfarmer.Properties.Resources.small_pig;
            this.mpbMarketR3.AnimalQuantity = 3;
            this.mpbMarketR3.AnimalToSell = this.mpbMarketL3;
            this.mpbMarketR3.AnimalType = Superfarmer.MarketPictureBox.Animals.pig;
            this.mpbMarketR3.BackColor = System.Drawing.Color.White;
            this.mpbMarketR3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mpbMarketR3.Dog = Superfarmer.MarketPictureBox.DogT.no;
            this.mpbMarketR3.Image = global::Superfarmer.Properties.Resources.small_pig_gray;
            this.mpbMarketR3.InactiveImage = global::Superfarmer.Properties.Resources.small_pig_gray;
            this.mpbMarketR3.Location = new System.Drawing.Point(212, 328);
            this.mpbMarketR3.Name = "mpbMarketR3";
            this.mpbMarketR3.Size = new System.Drawing.Size(61, 46);
            this.mpbMarketR3.TabIndex = 50;
            this.mpbMarketR3.TabStop = false;
            this.mpbMarketR3.Click += new System.EventHandler(this.PBMarketClick);
            // 
            // mpbMarketL3
            // 
            this.mpbMarketL3.Active = false;
            this.mpbMarketL3.ActiveImage = global::Superfarmer.Properties.Resources.small_cow;
            this.mpbMarketL3.AnimalQuantity = 1;
            this.mpbMarketL3.AnimalToSell = this.mpbMarketR3;
            this.mpbMarketL3.AnimalType = Superfarmer.MarketPictureBox.Animals.cow;
            this.mpbMarketL3.BackColor = System.Drawing.Color.White;
            this.mpbMarketL3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mpbMarketL3.Dog = Superfarmer.MarketPictureBox.DogT.no;
            this.mpbMarketL3.Image = global::Superfarmer.Properties.Resources.small_cow_gray;
            this.mpbMarketL3.InactiveImage = global::Superfarmer.Properties.Resources.small_cow_gray;
            this.mpbMarketL3.Location = new System.Drawing.Point(57, 328);
            this.mpbMarketL3.Name = "mpbMarketL3";
            this.mpbMarketL3.Size = new System.Drawing.Size(61, 46);
            this.mpbMarketL3.TabIndex = 43;
            this.mpbMarketL3.TabStop = false;
            this.mpbMarketL3.Click += new System.EventHandler(this.PBMarketClick);
            // 
            // labelMarketL1
            // 
            this.labelMarketL1.BackColor = System.Drawing.Color.White;
            this.labelMarketL1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMarketL1.Location = new System.Drawing.Point(5, 236);
            this.labelMarketL1.Name = "labelMarketL1";
            this.labelMarketL1.Size = new System.Drawing.Size(32, 46);
            this.labelMarketL1.TabIndex = 8;
            this.labelMarketL1.Text = "x";
            // 
            // mpbMarketR4
            // 
            this.mpbMarketR4.Active = false;
            this.mpbMarketR4.ActiveImage = global::Superfarmer.Properties.Resources.small_cow;
            this.mpbMarketR4.AnimalQuantity = 2;
            this.mpbMarketR4.AnimalToSell = this.mpbMarketL4;
            this.mpbMarketR4.AnimalType = Superfarmer.MarketPictureBox.Animals.cow;
            this.mpbMarketR4.BackColor = System.Drawing.Color.White;
            this.mpbMarketR4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mpbMarketR4.Dog = Superfarmer.MarketPictureBox.DogT.no;
            this.mpbMarketR4.Image = global::Superfarmer.Properties.Resources.small_cow_gray;
            this.mpbMarketR4.InactiveImage = global::Superfarmer.Properties.Resources.small_cow_gray;
            this.mpbMarketR4.Location = new System.Drawing.Point(212, 374);
            this.mpbMarketR4.Name = "mpbMarketR4";
            this.mpbMarketR4.Size = new System.Drawing.Size(61, 46);
            this.mpbMarketR4.TabIndex = 49;
            this.mpbMarketR4.TabStop = false;
            this.mpbMarketR4.Click += new System.EventHandler(this.PBMarketClick);
            // 
            // mpbMarketL4
            // 
            this.mpbMarketL4.Active = false;
            this.mpbMarketL4.ActiveImage = global::Superfarmer.Properties.Resources.small_horse;
            this.mpbMarketL4.AnimalQuantity = 1;
            this.mpbMarketL4.AnimalToSell = this.mpbMarketR4;
            this.mpbMarketL4.AnimalType = Superfarmer.MarketPictureBox.Animals.horse;
            this.mpbMarketL4.BackColor = System.Drawing.Color.White;
            this.mpbMarketL4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mpbMarketL4.Dog = Superfarmer.MarketPictureBox.DogT.no;
            this.mpbMarketL4.Image = global::Superfarmer.Properties.Resources.small_horse_gray;
            this.mpbMarketL4.InactiveImage = global::Superfarmer.Properties.Resources.small_horse_gray;
            this.mpbMarketL4.Location = new System.Drawing.Point(57, 374);
            this.mpbMarketL4.Name = "mpbMarketL4";
            this.mpbMarketL4.Size = new System.Drawing.Size(61, 46);
            this.mpbMarketL4.TabIndex = 44;
            this.mpbMarketL4.TabStop = false;
            this.mpbMarketL4.Click += new System.EventHandler(this.PBMarketClick);
            // 
            // mpbMarketR5
            // 
            this.mpbMarketR5.Active = false;
            this.mpbMarketR5.ActiveImage = global::Superfarmer.Properties.Resources.small_sheep;
            this.mpbMarketR5.AnimalQuantity = 1;
            this.mpbMarketR5.AnimalToSell = this.mpbMarketL5;
            this.mpbMarketR5.AnimalType = Superfarmer.MarketPictureBox.Animals.sheep;
            this.mpbMarketR5.BackColor = System.Drawing.Color.White;
            this.mpbMarketR5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mpbMarketR5.Dog = Superfarmer.MarketPictureBox.DogT.smallDogSell;
            this.mpbMarketR5.Image = global::Superfarmer.Properties.Resources.small_sheep_gray;
            this.mpbMarketR5.InactiveImage = global::Superfarmer.Properties.Resources.small_sheep_gray;
            this.mpbMarketR5.Location = new System.Drawing.Point(212, 420);
            this.mpbMarketR5.Name = "mpbMarketR5";
            this.mpbMarketR5.Size = new System.Drawing.Size(61, 46);
            this.mpbMarketR5.TabIndex = 48;
            this.mpbMarketR5.TabStop = false;
            this.mpbMarketR5.Click += new System.EventHandler(this.PBMarketClick);
            // 
            // mpbMarketL5
            // 
            this.mpbMarketL5.Active = false;
            this.mpbMarketL5.ActiveImage = global::Superfarmer.Properties.Resources.small_smalldog;
            this.mpbMarketL5.AnimalQuantity = 1;
            this.mpbMarketL5.AnimalToSell = this.mpbMarketR5;
            this.mpbMarketL5.AnimalType = Superfarmer.MarketPictureBox.Animals.other;
            this.mpbMarketL5.BackColor = System.Drawing.Color.White;
            this.mpbMarketL5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mpbMarketL5.Dog = Superfarmer.MarketPictureBox.DogT.smallDogBuy;
            this.mpbMarketL5.Image = global::Superfarmer.Properties.Resources.small_smalldog_gray;
            this.mpbMarketL5.InactiveImage = global::Superfarmer.Properties.Resources.small_smalldog_gray;
            this.mpbMarketL5.Location = new System.Drawing.Point(57, 420);
            this.mpbMarketL5.Name = "mpbMarketL5";
            this.mpbMarketL5.Size = new System.Drawing.Size(61, 46);
            this.mpbMarketL5.TabIndex = 45;
            this.mpbMarketL5.TabStop = false;
            this.mpbMarketL5.Click += new System.EventHandler(this.PBMarketClick);
            // 
            // labelMarketR1
            // 
            this.labelMarketR1.BackColor = System.Drawing.Color.White;
            this.labelMarketR1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMarketR1.Location = new System.Drawing.Point(168, 236);
            this.labelMarketR1.Name = "labelMarketR1";
            this.labelMarketR1.Size = new System.Drawing.Size(38, 46);
            this.labelMarketR1.TabIndex = 19;
            this.labelMarketR1.Text = "x";
            // 
            // mpbMarketL6
            // 
            this.mpbMarketL6.Active = false;
            this.mpbMarketL6.ActiveImage = global::Superfarmer.Properties.Resources.small_bigdog;
            this.mpbMarketL6.AnimalQuantity = 1;
            this.mpbMarketL6.AnimalToSell = this.mpbMarketR6;
            this.mpbMarketL6.AnimalType = Superfarmer.MarketPictureBox.Animals.other;
            this.mpbMarketL6.BackColor = System.Drawing.Color.White;
            this.mpbMarketL6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mpbMarketL6.Dog = Superfarmer.MarketPictureBox.DogT.bigDogBuy;
            this.mpbMarketL6.Image = global::Superfarmer.Properties.Resources.small_bigdog_gray;
            this.mpbMarketL6.InactiveImage = global::Superfarmer.Properties.Resources.small_bigdog_gray;
            this.mpbMarketL6.Location = new System.Drawing.Point(57, 466);
            this.mpbMarketL6.Name = "mpbMarketL6";
            this.mpbMarketL6.Size = new System.Drawing.Size(61, 46);
            this.mpbMarketL6.TabIndex = 46;
            this.mpbMarketL6.TabStop = false;
            this.mpbMarketL6.Click += new System.EventHandler(this.PBMarketClick);
            // 
            // mpbMarketR6
            // 
            this.mpbMarketR6.Active = false;
            this.mpbMarketR6.ActiveImage = global::Superfarmer.Properties.Resources.small_cow;
            this.mpbMarketR6.AnimalQuantity = 1;
            this.mpbMarketR6.AnimalToSell = this.mpbMarketL6;
            this.mpbMarketR6.AnimalType = Superfarmer.MarketPictureBox.Animals.cow;
            this.mpbMarketR6.BackColor = System.Drawing.Color.White;
            this.mpbMarketR6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mpbMarketR6.Dog = Superfarmer.MarketPictureBox.DogT.bigDogSell;
            this.mpbMarketR6.Image = global::Superfarmer.Properties.Resources.small_cow_gray;
            this.mpbMarketR6.InactiveImage = global::Superfarmer.Properties.Resources.small_cow_gray;
            this.mpbMarketR6.InitialImage = null;
            this.mpbMarketR6.Location = new System.Drawing.Point(212, 466);
            this.mpbMarketR6.Name = "mpbMarketR6";
            this.mpbMarketR6.Size = new System.Drawing.Size(61, 46);
            this.mpbMarketR6.TabIndex = 47;
            this.mpbMarketR6.TabStop = false;
            this.mpbMarketR6.Click += new System.EventHandler(this.PBMarketClick);
            // 
            // labelMarketR6
            // 
            this.labelMarketR6.BackColor = System.Drawing.Color.White;
            this.labelMarketR6.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMarketR6.Location = new System.Drawing.Point(168, 466);
            this.labelMarketR6.Name = "labelMarketR6";
            this.labelMarketR6.Size = new System.Drawing.Size(38, 46);
            this.labelMarketR6.TabIndex = 34;
            this.labelMarketR6.Text = "x";
            // 
            // labelMarketL6
            // 
            this.labelMarketL6.BackColor = System.Drawing.Color.White;
            this.labelMarketL6.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMarketL6.Location = new System.Drawing.Point(5, 466);
            this.labelMarketL6.Name = "labelMarketL6";
            this.labelMarketL6.Size = new System.Drawing.Size(32, 46);
            this.labelMarketL6.TabIndex = 16;
            this.labelMarketL6.Text = "x";
            // 
            // labelMarketR5
            // 
            this.labelMarketR5.BackColor = System.Drawing.Color.White;
            this.labelMarketR5.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMarketR5.Location = new System.Drawing.Point(168, 420);
            this.labelMarketR5.Name = "labelMarketR5";
            this.labelMarketR5.Size = new System.Drawing.Size(38, 46);
            this.labelMarketR5.TabIndex = 31;
            this.labelMarketR5.Text = "x";
            // 
            // labelMarketL5
            // 
            this.labelMarketL5.BackColor = System.Drawing.Color.White;
            this.labelMarketL5.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMarketL5.Location = new System.Drawing.Point(5, 420);
            this.labelMarketL5.Name = "labelMarketL5";
            this.labelMarketL5.Size = new System.Drawing.Size(32, 46);
            this.labelMarketL5.TabIndex = 15;
            this.labelMarketL5.Text = "x";
            // 
            // labelMarketR4
            // 
            this.labelMarketR4.BackColor = System.Drawing.Color.White;
            this.labelMarketR4.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMarketR4.Location = new System.Drawing.Point(168, 374);
            this.labelMarketR4.Name = "labelMarketR4";
            this.labelMarketR4.Size = new System.Drawing.Size(38, 46);
            this.labelMarketR4.TabIndex = 28;
            this.labelMarketR4.Text = "x";
            // 
            // labelMarketL4
            // 
            this.labelMarketL4.BackColor = System.Drawing.Color.White;
            this.labelMarketL4.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMarketL4.Location = new System.Drawing.Point(5, 374);
            this.labelMarketL4.Name = "labelMarketL4";
            this.labelMarketL4.Size = new System.Drawing.Size(32, 46);
            this.labelMarketL4.TabIndex = 14;
            this.labelMarketL4.Text = "x";
            // 
            // labelMarketR3
            // 
            this.labelMarketR3.BackColor = System.Drawing.Color.White;
            this.labelMarketR3.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMarketR3.Location = new System.Drawing.Point(168, 328);
            this.labelMarketR3.Name = "labelMarketR3";
            this.labelMarketR3.Size = new System.Drawing.Size(38, 46);
            this.labelMarketR3.TabIndex = 25;
            this.labelMarketR3.Text = "x";
            // 
            // labelMarketL3
            // 
            this.labelMarketL3.BackColor = System.Drawing.Color.White;
            this.labelMarketL3.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMarketL3.Location = new System.Drawing.Point(5, 328);
            this.labelMarketL3.Name = "labelMarketL3";
            this.labelMarketL3.Size = new System.Drawing.Size(32, 46);
            this.labelMarketL3.TabIndex = 13;
            this.labelMarketL3.Text = "x";
            // 
            // labelMarketR2
            // 
            this.labelMarketR2.BackColor = System.Drawing.Color.White;
            this.labelMarketR2.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMarketR2.Location = new System.Drawing.Point(168, 282);
            this.labelMarketR2.Name = "labelMarketR2";
            this.labelMarketR2.Size = new System.Drawing.Size(38, 46);
            this.labelMarketR2.TabIndex = 22;
            this.labelMarketR2.Text = "x";
            // 
            // labelMarketL2
            // 
            this.labelMarketL2.BackColor = System.Drawing.Color.White;
            this.labelMarketL2.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMarketL2.Location = new System.Drawing.Point(5, 282);
            this.labelMarketL2.Name = "labelMarketL2";
            this.labelMarketL2.Size = new System.Drawing.Size(32, 46);
            this.labelMarketL2.TabIndex = 12;
            this.labelMarketL2.Text = "x";
            // 
            // panelMarket1
            // 
            this.panelMarket1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panelMarket1.BackColor = System.Drawing.Color.Silver;
            this.panelMarket1.Controls.Add(this.panelMarket2);
            this.panelMarket1.Location = new System.Drawing.Point(223, 12);
            this.panelMarket1.Name = "panelMarket1";
            this.panelMarket1.Size = new System.Drawing.Size(322, 537);
            this.panelMarket1.TabIndex = 0;
            // 
            // playerTable
            // 
            this.playerTable.AutoSize = true;
            this.playerTable.BackColor = System.Drawing.Color.White;
            this.playerTable.BigDog = false;
            this.playerTable.Cow = 0;
            this.playerTable.Horse = 0;
            this.playerTable.Location = new System.Drawing.Point(12, 12);
            this.playerTable.Name = "playerTable";
            this.playerTable.Pig = 0;
            this.playerTable.Rabbit = 0;
            this.playerTable.Sheep = 0;
            this.playerTable.Size = new System.Drawing.Size(206, 509);
            this.playerTable.SmallDog = false;
            this.playerTable.TabIndex = 5;
            // 
            // Board
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(554, 592);
            this.ControlBox = false;
            this.Controls.Add(this.playerTable);
            this.Controls.Add(this.buttonRules);
            this.Controls.Add(this.buttonRestart);
            this.Controls.Add(this.buttonQuit);
            this.Controls.Add(this.panelMarket1);
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = global::Superfarmer.Properties.Resources.icon_main;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(570, 631);
            this.Name = "Board";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "superfarmer";
            this.panelMarket2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbDice1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDice2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketR2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketL2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketR1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketL1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketR3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketL3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketR4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketL4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketR5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketL5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketL6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbMarketR6)).EndInit();
            this.panelMarket1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

        private System.Windows.Forms.Button buttonRules;
        private PlayerAnimals playerTable;
        private System.Windows.Forms.Button buttonRestart;
        private System.Windows.Forms.Button buttonQuit;
        private System.Windows.Forms.Panel panelMarket2;
        private System.Windows.Forms.Label labelMarketC6;
        private System.Windows.Forms.Label labelMarketC2;
        private System.Windows.Forms.PictureBox pbDice1;
        private System.Windows.Forms.Label labelMarketC4;
        private System.Windows.Forms.Label labelMarketC3;
        private System.Windows.Forms.Label labelMarketC5;
        private System.Windows.Forms.PictureBox pbDice2;
        private System.Windows.Forms.Label labelDice1;
        private System.Windows.Forms.Label labelDice2;
        private System.Windows.Forms.Button buttonRoll;
        private MarketPictureBox mpbMarketR2;
        private MarketPictureBox mpbMarketL2;
        private System.Windows.Forms.Label labelMarketC1;
        private MarketPictureBox mpbMarketR1;
        private MarketPictureBox mpbMarketL1;
        private MarketPictureBox mpbMarketR3;
        private MarketPictureBox mpbMarketL3;
        private System.Windows.Forms.Label labelMarketL1;
        private MarketPictureBox mpbMarketR4;
        private MarketPictureBox mpbMarketL4;
        private MarketPictureBox mpbMarketR5;
        private MarketPictureBox mpbMarketL5;
        private System.Windows.Forms.Label labelMarketR1;
        private MarketPictureBox mpbMarketL6;
        private MarketPictureBox mpbMarketR6;
        private System.Windows.Forms.Label labelMarketR6;
        private System.Windows.Forms.Label labelMarketL6;
        private System.Windows.Forms.Label labelMarketR5;
        private System.Windows.Forms.Label labelMarketL5;
        private System.Windows.Forms.Label labelMarketR4;
        private System.Windows.Forms.Label labelMarketL4;
        private System.Windows.Forms.Label labelMarketR3;
        private System.Windows.Forms.Label labelMarketL3;
        private System.Windows.Forms.Label labelMarketR2;
        private System.Windows.Forms.Label labelMarketL2;
        private System.Windows.Forms.Panel panelMarket1;
    }
}
